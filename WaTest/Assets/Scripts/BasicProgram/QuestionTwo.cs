﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionTwo : MonoBehaviour
{
    [SerializeField] private InputField _content;
    [SerializeField] private Text _result;

    private float _cornerEachMinus;
    private int _numberCorner = 2; // one hour, hour hand and minus hand can create 2 same corner
    private float _cornerInput;

    // Start is called before the first frame update
    void Start()
    {
        _cornerEachMinus = 360f / 60f;// 1 minus on clock is 6
    }

    public void ClickCaculateBtn()
    {
        if(string.IsNullOrEmpty(_content.text))
        {
            _result.text = "<color=red>Please Input value!</color>";
            return;
        }

        _cornerInput = float.Parse(_content.text);

        if(_cornerInput < 0 || _cornerInput > 180)
        {
            _result.text = "<color=red>Please Input value from 0 to 180!</color>";
            return;
        }

        float numberMinus = _cornerInput / _cornerEachMinus; // convert from the corner to corresponding the minus
        Debug.Log("numberMinus ----> " + numberMinus);
        numberMinus *= _numberCorner;// have 2 times each hour that hour hand and minus hand can create same corner
        Debug.Log("numberMinus 1----> " + numberMinus);

        numberMinus *= 24f; // have 24 hour each day
        Debug.Log("numberMinus 2----> " + numberMinus);
        
        string strResult = string.Format("{0 :00.00}" , numberMinus * 60f);
        _result.text = strResult;
    }
}
