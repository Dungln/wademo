﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    [SerializeField] private CarMove _car;

    public Button BtnAcceleration;
    public Button BtnDecceleration;
    public Button BtnPlayAgain;
    
    // Start is called before the first frame update
    void Start()
    {
        BtnAcceleration.onClick.AddListener(StartAcceleration);
        BtnDecceleration.onClick.AddListener(StartDecceleration);
        BtnPlayAgain.onClick.AddListener(PlayAgain);
    }

    private void Update()
    {
        if (_car.IsCompletePath)
        {
            BtnPlayAgain.gameObject.SetActive(true);
        }
    }

    public void StartAcceleration()
    {
        if (_car.IsDecceleration)
        {
            _car.IsDecceleration = false;
            BtnDecceleration.transform.GetComponent<Image>().color = Color.white;
        }

        if (!_car.IsAcceleration)
        {
            _car.IsAcceleration = true;
            BtnAcceleration.transform.GetComponent<Image>().color = Color.gray;
        }
        else
        {
            _car.IsAcceleration = false;
            BtnAcceleration.transform.GetComponent<Image>().color = Color.white;
        }
    }

    public void StartDecceleration()
    {
        if (_car.IsDecceleration)
        {
            _car.IsAcceleration = false;
            BtnAcceleration.transform.GetComponent<Image>().color = Color.white;
        }

        if (!_car.IsDecceleration)
        {
            _car.IsDecceleration = true;
            BtnDecceleration.transform.GetComponent<Image>().color = Color.gray;
        }
        else
        {
            _car.IsDecceleration = false;
            BtnDecceleration.transform.GetComponent<Image>().color = Color.white;
        }
    }

    public void PlayAgain()
    {
        _car.ResetInfo();
        _car.CreatePath();
        BtnPlayAgain.gameObject.SetActive(false);
    }
}
