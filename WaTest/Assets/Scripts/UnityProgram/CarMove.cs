﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarMove : MonoBehaviour
{
    public Transform[] pointsPath1;
    public Transform[] pointsPath2;
    public Transform[] pointsPath3;
    public Transform[] pointsPath4;
    public Transform[] pointsPath5;
    public Transform[] pointsPath6;

    public Transform[] _currentPoint;
 
    public List<Transform[]> PointsPath = new List<Transform[]>();

    public int desPoint = 0;
    public float CurrentSpeed = 0;
    public float MaxSpeed = 200f;

    public float Acceleration = 10f;
    public float Decceleration = 10f;

    public bool IsAcceleration = false;
    public bool IsDecceleration = false;
    public bool IsCompletePath = false;

    private NavMeshAgent _agent;
    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(1).GetComponent<MeshRenderer>().material.color = Color.blue;

        _agent = this.transform.GetComponent<NavMeshAgent>();
        PointsPath.Add(pointsPath1);
        PointsPath.Add(pointsPath2);
        PointsPath.Add(pointsPath3);
        PointsPath.Add(pointsPath4);
        PointsPath.Add(pointsPath5);
        PointsPath.Add(pointsPath6);

        CreatePath();
    }

    void GoNextPoint()
    {
        if (_currentPoint.Length == 0 || desPoint >= _currentPoint.Length)
        {
            IsCompletePath = true;
            return;
        }
        Vector3 direction = transform.TransformDirection(_currentPoint[desPoint].position);
        Vector3 directionTarget = _currentPoint[desPoint].position - this.transform.position;

        Quaternion targetRotation = Quaternion.LookRotation(directionTarget);
        this.transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, CurrentSpeed * Time.deltaTime);

        float distance = Vector3.Distance(_currentPoint[desPoint].position, transform.position);

        if(distance > 0.1f)
        {
            Vector3 movement = directionTarget.normalized * CurrentSpeed * Time.deltaTime;
            _agent.Move(movement);
        }

        if(distance <= 1.2f)
        {
            _currentPoint[desPoint].GetComponent<MeshRenderer>().material.color = Color.green;
            desPoint++;
        }

    }

    // Update is called once per frame
    void Update()
    {
        GoNextPoint();

        if (IsAcceleration)
        {
            IncreaseSpeed();
        }

        if (IsDecceleration)
        {
            DecreaseSpeed();
        }

    }

    public void CreatePath()
    {
        var indexPath = UnityEngine.Random.Range(0, PointsPath.Count);
        _currentPoint = PointsPath[indexPath];

        for (int i = 0; i < _currentPoint.Length; i++)
        {
            _currentPoint[i].GetComponent<MeshRenderer>().material.color = Color.red;
        }

        _agent.autoBraking = false;
        this.transform.position = _currentPoint[desPoint].position;
        desPoint++;
    }

    public void ResetInfo()
    {
        for(int i = 0; i < _currentPoint.Length; i++)
        {
            _currentPoint[i].GetComponent<MeshRenderer>().material.color = Color.white;
        }

        IsCompletePath = false;
        desPoint = 0;
        _currentPoint = null;
        CurrentSpeed = 0;
    }

    public void IncreaseSpeed()
    {
        if(CurrentSpeed <= MaxSpeed)
        {
            CurrentSpeed += Acceleration * Time.deltaTime;
        }
    }

    public void DecreaseSpeed()
    {
        if(CurrentSpeed >= 0)
        {
            CurrentSpeed -= Decceleration * Time.deltaTime;
        }
        else
        {
            CurrentSpeed = 0;
        }

    }


}
